
export function Enemy(armor, x, y, vida)
{
    let scale = 2;
    let frameRate = 5;
    let sprite;
    let player_armor = armor;
    let hp = vida;



    function create(context)
    {
        fetch(`public/assets/sprites/${player_armor}.json`).then(response =>
        {
            response.json().then(spritesheet =>
            {
                context.load.spritesheet(player_armor, `public/assets/img/${player_armor}.png`, { frameWidth: spritesheet.width, frameHeight: spritesheet.height });
            })

        }).catch(err =>
        {
            alert("Erro ao carregar armadura do jogador.");
        })

    }

    function setHp(newHp)
    {
        hp = newHp;
    }

    function getHp()
    {
        return hp;
    }

    function setFrameRate(newFrame)
    {
        frameRate = newFrame;
    }

    function setPositionX(positionX)
    {
        sprite.x = Math.floor(positionX);
    }

    function setPositionY(positionY)
    {
        sprite.y = Math.floor(positionY);
    }

    function getPositionX()
    {
        return sprite.x;
    }

    function getPositionY()
    {
        return sprite.y;
    }

    function getSprite()
    {
        return sprite;
    }


    function configPlayer(context)
    {
        createArmadura(context);
        sprite = context.add.container(x, y,
            [
                context.add.sprite(0, 0, player_armor),
            ]);
        playAnimation('idle_down');
        // console.log(sprite);


    }

    function createArmadura(context)
    {
        fetch(`public/assets/sprites/${player_armor}.json`).then(response =>
        {
            response.json().then(ar =>
            {
                for (let armadura in ar.animations) {

                    let length = ar.animations[armadura].length - 1;
                    let start = ar.animations[armadura].row * 5;
                    context.anims.create({
                        key: armadura + player_armor,
                        frameWidth: ar.width,
                        frameHeight: ar.height,
                        frames: context.anims.generateFrameNames(player_armor, { start: start, end: start + length }),
                        repeat: -1,
                        frameRate: frameRate
                    });
                }



            });

        }).catch(err =>
        {
            alert("Erro ao carregar armadura do jogador.");
        })
    }


    function playAnimation(animation)
    {
        sprite.list[0].play(animation + player_armor);
    }

    function stopAnimation(animation)
    {
        sprite.list[0].anims.stop(null, true);
    }

    return {
        create,
        setFrameRate,
        configPlayer,
        getSprite,
        playAnimation,
        stopAnimation,

        getPositionX,
        getPositionY,
        setPositionX,
        setPositionY,
        setHp,
        getHp
    }
}