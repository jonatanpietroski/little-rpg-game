
export function Player(armor, weapon)
{
    let scale = 2;
    let frameRate = 5;
    let sprite;
    let tmp;
    let player_weapon = weapon;
    let player_armor = armor;
    let atackInterval = [];
    let colliderActivated = false;
    let strength;



    function create(context)
    {
        context.load.spritesheet(player_armor, `public/assets/img/${player_armor}.png`, { frameWidth: 64, frameHeight: 64 });
        context.load.spritesheet(player_weapon, `public/assets/img/${player_weapon}.png`, { frameWidth: 64, frameHeight: 64 });
    }

    function setFrameRate(newFrame)
    {
        frameRate = newFrame;
    }

    function setPositionX(positionX)
    {
        sprite.x = Math.floor(positionX);
    }

    function setPositionY(positionY)
    {
        sprite.y = Math.floor(positionY);
    }

    function getPositionX()
    {
        return sprite.x;
    }

    function getPositionY()
    {
        return sprite.y;
    }

    function getSprite()
    {
        return sprite;
    }

    function getColliderStatus()
    {
        return colliderActivated;
    }

    function setColliderStatus(status)
    {
        colliderActivated = status;
    }

    function setStrength(newStrength)
    {
        strength = newStrength;
    }

    function getStrength()
    {
        return strength;
    }

    function configPlayer(context)
    {
        createArmadura(context);
        createWeapon(context);
        sprite = context.add.container(300, 400,
            [
                context.physics.add.sprite(0, 0, player_armor),
                context.physics.add.sprite(0, 0, player_weapon)
            ]);

        sprite.list[0].setSize(32, 42, true);
        sprite.list[0].setCollideWorldBounds(true);
        sprite.list[1].setCollideWorldBounds(true);
        playAnimation('idle_down');
        // console.log(sprite);


    }

    function createArmadura(context)
    {
        fetch(`public/assets/sprites/${player_armor}.json`).then(response =>
        {
            response.json().then(ar =>
            {
                for (let armadura in ar.animations) {

                    let length = ar.animations[armadura].length - 1;
                    let start = ar.animations[armadura].row * 5;
                    context.anims.create({
                        key: armadura + player_armor,
                        frameWidth: 64,
                        frameHeight: 64,
                        frames: context.anims.generateFrameNames(player_armor, { start: start, end: start + length }),
                        repeat: -1,
                        frameRate: frameRate
                    });
                }



            });

        }).catch(err =>
        {
            alert("Erro ao carregar armadura do jogador.");
        })
    }

    function createWeapon(context)
    {
        fetch(`public/assets/sprites/${player_weapon}.json`).then(response =>
        {
            response.json().then(ar =>
            {
                strength = ar.strength;
                for (let pweapon in ar.animations) {

                    let length = ar.animations[pweapon].length - 1;
                    let start = ar.animations[pweapon].row * 5;


                    context.anims.create({
                        key: pweapon + player_weapon,
                        frameWidth: 64,
                        frameHeight: 64,
                        frames: context.anims.generateFrameNames(player_weapon, { start: start, end: start + length }),
                        repeat: -1,
                        frameRate: frameRate
                    });
                }



            });

        }).catch(err =>
        {
            alert("Erro ao carregar arma do jogador.");
        })
    }

    function atack()
    {
        atackInterval = [];
        console.log(sprite.list[1].body);
        playAnimation('atk_right');
        if (sprite.list[1].anims.currentAnim.key.includes('atk')) {
            atackInterval[0] = setInterval(() =>
            {

                sprite.list[1].body.setSize(16, 16);
                sprite.list[1].body.setOffset(50, 24);
                setColliderStatus(true);

            }, 500);
            atackInterval[1] = setInterval(() =>
            {
                setColliderStatus(false);
                resetBodySize()

            }, 1000);
        }
    }

    function resetBodySize()
    {
        sprite.list[1].body.setSize(0, 0);
        sprite.list[1].body.setOffset(0, 0);
    }


    function stopAtackInterval()
    {
        clearInterval(atackInterval[0]);
        clearInterval(atackInterval[1]);
        setColliderStatus(false);
        resetBodySize();
    }

    function playAnimation(animation)
    {
        stopAtackInterval();
        sprite.list[0].play(animation + player_armor);
        sprite.list[1].play(animation + player_weapon);

    }

    function stopAnimation(animation)
    {
        sprite.list[0].anims.stop(null, true);
        sprite.list[1].anims.stop(null, true);
    }

    return {
        create,
        configPlayer,
        atack,
        playAnimation,
        stopAnimation,

        getSprite,
        setFrameRate,
        getPositionX,
        getPositionY,
        setPositionX,
        setPositionY,
        getColliderStatus,
        setColliderStatus,
        setStrength,
        getStrength
    }
}