var config = {
    width: window.innerWidth,
    height: window.innerHeight,
    type: Phaser.AUTO,
    parent: 'game-entry',
    scene: {
        create: create,
        preload: preload,
        update: update
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                y: 0
            },
            debug: true
        }
    }
};

import { Player } from './player.js'
import { Enemy } from './enemy.js';

var game = new Phaser.Game(config);

var camera;
let mapaColisao;
let keys;
let mapDPosition = 32;

var player = new Player('clotharmor', 'sword1');
var enemy = new Enemy('bat', 300, 300, 100);
window.enemy = enemy;
window.player = player;


function preload()
{
    // Map
    this.load.image("zoltan-town", "public/assets/img/tilesheet.png");
    this.load.tilemapTiledJSON("zoltan", "public/assets/maps/zoltan.json");

    // Audio
    this.load.audio('zoltan-town-music', [
        'public/assets/audio/towns/zoltan.ogg',
        'public/assets/audio/towns/zoltan.mp3',
    ]);

    // Sprites
    player.create(this);
    enemy.create(this);

}


function configAudio(context)
{
    var music = context.sound.add('zoltan-town-music', {
        mute: false,
        volume: 1,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
    });

    music.play();
}

function configMap(context)
{
    const map = context.make.tilemap({ key: "zoltan", tileWidth: mapDPosition, tileHeight: mapDPosition });
    const tileset = map.addTilesetImage("zoltan-town");
    const layer = map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    mapaColisao = map.createStaticLayer(1, tileset, 0, 0); // layer index, tileset, x, y
    const layer3 = map.createStaticLayer(2, tileset, 0, 0); // layer index, tileset, x, y
    context.physics.add.existing(mapaColisao);
}



function create()
{
    // setCamera(this);
    // configAudio(this);
    configMap(this);
    player.configPlayer(this);
    enemy.configPlayer(this);
    keys = this.input.keyboard.addKeys('W');


    // Adiciona o sprite da arma do jogador aos elementos físicos do jogo.
    this.physics.add.existing(player.getSprite().list[1]);
    // Adiciona o sprite do inimigo aos elementos físicos do jogo.
    this.physics.add.existing(enemy.getSprite().list[0]);

    // Adiciona um Listener de colisões entre a espada e o inimigo.
    this.physics.add.collider(player.getSprite().list[1], enemy.getSprite().list[0],
        function (p, e)
        {
            // Muda o status de colisão para falso para colidir apenas 1 vez
            player.setColliderStatus(false);
            // Verifica se o HP do inimigo chegou a 0
            if (enemy.getHp() - player.getStrength() > 0) {
                enemy.setHp(enemy.getHp() - player.getStrength());
            } else {
                // Se o HP do Inimigo Zerar ele destroi o inimigo.
                e.destroy();
            }

        },
        function ()
        {
            // Se retornar falso não realiza colisão, isso previne que a função acima seja chamada multiplas vezes seguidas.
            return player.getColliderStatus();
        }, this);

    // this.physics.add.overlap(player.getSprite().list[0], player.getSprite().list[0]);

    this.input.on('pointerdown', function (pointer)
    {
        player.playAnimation('idle_down')

        let pathFindingManager = new Pathfinding.PathFinding();
        pathFindingManager.setWalkable(0) // or this.pathFindingManager.setWalkable(0, 10, 11); 
        let col = Math.floor(player.getPositionY() / mapDPosition);
        let row = Math.floor(player.getPositionX() / mapDPosition);
        pathFindingManager.setStart({ col: col, row: row });
        pathFindingManager.setEnd({ col: Math.floor(pointer.worldY / mapDPosition), row: Math.floor(pointer.worldX / mapDPosition) });

        // console.log("FROM", { col: col, row: row });

        // console.log("TO", { col: Math.floor(pointer.worldY / mapDPosition), row: Math.floor(pointer.worldX / mapDPosition) });


        let mapinha = [];

        let mapaColisao = game.cache.tilemap.get('zoltan').data.layers[1].chunks;

        let tmp = game.cache.tilemap.get('zoltan').data.layers[1].chunks.length;
        for (let i = 0; i < tmp; i++) {
            mapinha.push(mapaColisao[i].data);
        }


        // console.log(mapinha);


        let bestPath = pathFindingManager.find(mapinha);
        for (let i = 0; i < bestPath.length; i++) {

            setTimeout(function ()
            {
                player.setPositionX(bestPath[i].row * mapDPosition);
                player.setPositionY(bestPath[i].col * mapDPosition);
            }, 200 * i);
        }


    }, this);






}

function update()
{

}